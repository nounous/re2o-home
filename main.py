#!/usr/bin/env python3
from configparser import ConfigParser
import socket

from re2oapi import Re2oAPIClient

import os
import shutil
import grp
import sys

path = (os.path.dirname(os.path.abspath(__file__)))

config = ConfigParser()
config.read(path+'/config.ini')

api_hostname = config.get('Re2o', 'hostname')
api_password = config.get('Re2o', 'password')
api_username = config.get('Re2o', 'username')

api_client = Re2oAPIClient(api_hostname,api_username,api_password, use_tls=False)

client_hostname = socket.gethostname().split('.',1)[0]

def reconfigure(api_client):

    users = api_client.list("users/homecreation")

    error = False
    os.chdir("/home-adh")
    for user in users:
        home = '/home-adh/{}/{}'.format(user['pseudo'][0].lower(),user['pseudo'])
        uid = user['uid']
        gid = user['gid']

        if not os.path.exists(home):  # Home dosen't exist, create it
            users_dirs = []
            for dirname in os.listdir("/home-adh"):
                if os.stat("/home-adh/" + dirname).st_uid == uid and os.path.islink("/home-adh/" + dirname):
                    users_dirs.append(dirname)

            if len(users_dirs) == 1:
            # This user had already a home directory before, renaming it
                # Renaming home directory
                shutil.move("/home-adh/" + users_dirs[0][0].lower() + '/' + users_dirs[0], home)
                os.chown(home,int(uid),int(gid))
                os.chown(home + '/Mail', int(uid), int(gid))
                # Deleting the symlink, will be recreated later
                os.remove("/home-adh/" + users_dirs[0])
                # Renaming the maildir
                os.rename("/home-adh/mail/" + users_dirs[0], '/home-adh/mail/' + user['pseudo']) 
                os.chown('/home-adh/mail/' + user['pseudo'], int(uid), 8)

            elif len(users_dirs) == 0:
                os.makedirs(home,0o701)
                os.chown(home,int(uid),int(gid))
        
        # Mail
        if not(os.path.exists(home + '/Mail')) and not(os.path.islink(home + '/Mail')):
            os.makedirs(home + '/Mail', 0o700)
            os.chown(home + '/Mail', int(uid), int(gid))
        if not(os.path.exists('/home-adh/mail/' + user['pseudo'])) and not(os.path.islink(home + '/Mail')):
            os.makedirs('/home-adh/mail/' + user['pseudo'], 0o700)
            os.chown('/home-adh/mail/' + user['pseudo'], int(uid), 8)

        # Owncloud dans le home
        #if not(os.path.exists(home + '/OwnCloud')) and not(os.path.islink(home + '/OwnCloud')):
        #    os.makedirs(home + '/OwnCloud')
        #    os.chown(home + '/OwnCloud',int(uid),grp.getgrnam('user').gr_gid)
        #    os.chmod(home + '/OwnCloud', 0o700)

        # Simlink
        link = '/home-adh/{}'.format(user['pseudo'])
        if not os.path.islink(link):
            link2 = '{}'.format(user['pseudo'])
            home2 = '{}/{}'.format(user['pseudo'][0].lower(),user['pseudo'])
            os.symlink(home2, link2)

        if not ((os.path.exists(home+'/Mail') or os.path.islink(home + '/Mail')) and (os.path.exists(home+'/OwnCloud') or os.path.islink(home + '/OwnCloud')) and os.path.islink(link)):
            print('creation du home de {}'.format(user['pseudo']))
            print("error: {}".format(error))
            error = True

    return error

for arg in sys.argv:
    if arg=="--force":
        reconfigure(api_client)

for service in api_client.list("services/regen/"):
    if service['hostname'] == client_hostname and \
        service['service_name'] == 'home' and \
        service['need_regen']:
        error = reconfigure(api_client)
        api_client.patch(service['api_url'], data={'need_regen': error})  # Regen is there is an error
